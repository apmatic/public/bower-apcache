'use strict';

angular.module('ap.cache', ['ng', 'angular-cache']).service('$cache', function(CacheFactory, $http) {
  var tables = CacheFactory('tables', {
    maxAge: 14400000, // 4h
    deleteOnExpire: 'aggressive',
    storageMode: 'localStorage'
  });

  var menus = CacheFactory('menus', {
    maxAge: 14400000, // 4h
    deleteOnExpire: 'aggressive',
    storageMode: 'localStorage',
    onExpire: function(key, value){
      $http.get('/menus').then(function (res) {
        menus.put(key, res.data);
      });
    }
  });

  return {
    menus: menus,
    tables: tables
  };
});